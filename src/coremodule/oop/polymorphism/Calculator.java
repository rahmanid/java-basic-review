package coremodule.oop.polymorphism;

public class Calculator {
    // polymorphism --- having many form
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.addition(10,10,30);
        calculator.addition(15,25);
        calculator.addition1(30,35);


    }
    // method overloading --same method,different parameter,same class
    // known as static polymorphism/compile time polymorphism
    public void addition(int x,int y,int z) {
        System.out.println(x + y + z);
        // can not create same parameterized method name
        // can create different parameterized method name
        // can create same parameter but different method name

    }
    public void addition(int x,int y){
        System.out.println(x+y);
    }
    public void addition1(int x,int y){
        System.out.println(x+y);
    }
    // method overriding --same method ,same parameter,different class
    // known as dynamic polymorphism/run time polymorphism



}
