package coremodule.oop.inharitance;

public class C extends B {

    public static void main(String[] args) {
        C c = new C();
        c.methodFromA();
        c.methodFromB();
    }
}
