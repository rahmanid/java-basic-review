package coremodule.oop.encapsulation;

public class EmployeeExecution {

    public static void main(String[] args) {

        Employee employee = new Employee();
        employee.setName("Ahnaf");
        String empName = employee.getName();
        System.out.println("Employee name is : " + empName);
        employee.setId("0291A");
        String empid = employee.getId();
        System.out.println("Employee id is : " + empid);
        employee.setSalary(12000);
        int empsalary = employee.getSalary();
        employee.setBonus(2000);
        int empbonus = employee.getBonus();



        System.out.println("Employee salary is : " + empsalary);
        System.out.println("Employee bonus is  : " + empbonus);
    }
}
