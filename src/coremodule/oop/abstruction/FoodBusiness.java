package coremodule.oop.abstruction;

public abstract class FoodBusiness {

    // abstract class can have method with/without body
    // abstract keyword need to be specified in abstract class
    // it can have constructor
    public abstract void cooler();
    public abstract void rac();
    public abstract void knife();

    public void name(){
        System.out.println("Opu Supermarket");

    }
}
