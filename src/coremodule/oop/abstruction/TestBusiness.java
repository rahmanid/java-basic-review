package coremodule.oop.abstruction;

public class TestBusiness {

    public static void main(String[] args) {
        Grocery grocery = new Grocery();
        grocery.name();
        grocery.cooler();
        grocery.fruit();
        grocery.knife();
        grocery.meat();
        grocery.rac();
        grocery.vegetable();
        String nm = grocery.name2();
        System.out.println(nm);

        Grocery2 grocery2 = new Grocery2();
        grocery2.frezer();
        grocery2.frozenfish();
        grocery2.meatcuttermachine();

    }
}
