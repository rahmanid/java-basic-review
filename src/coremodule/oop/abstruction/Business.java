package coremodule.oop.abstruction;

public interface Business {

    // interface is just an concept

    public void fruit();
    public void vegetable();
    public void meat();
    public String name2();

    // can't have method with body
    //can't have constructor
    //can have return/void type method names--> not body
}
