package coremodule.oop.abstruction;

public class Grocery extends FoodBusiness implements Business {

    @Override
    public void fruit() {
        System.out.println("need all kinds of fruit");

    }

    @Override
    public void vegetable() {
        System.out.println("need vegetable for sale");

    }

    @Override
    public void meat() {
        System.out.println("need beef,goat and chicken");

    }

    @Override
    public void cooler() {
        System.out.println("need walking cooler");

    }

    @Override
    public void rac() {
        System.out.println("need rac for keeping grocery items");

    }

    @Override
    public void knife() {
        System.out.println("need to meat cutting knife");

    }

    @Override
    public String name2() {
        return "Opu Supermarket2";
    }
}
