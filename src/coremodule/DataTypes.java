package coremodule;

public class DataTypes {
   static String name = "my name is khan";    //this is called global variable/class level variable
     int number = 2019;                       // global variable can call any where (if it is static)

    public DataTypes() {                    // if you want to call global variable need to create a constructor.
                                               // constructor is same as class name
    }
    // void method
    // void method perform action not return anything
    public void printMyName(){
            System.out.println("My Name Abid");
    }
    //return type method
    public String getMyName(){
        String name = "my name is Ahnaf";
        return name;
    }
    public int getNumber(){
        int num = 1000;
        return num;
    }
    // if i need to call this two method inside the main method or any other class
    // need to create an object

    public static void main(String[] args) {
        // String name = "Khan"; // this variable is belongs to this method only.
        DataTypes dataTypes = new DataTypes();   // first DataTypes is a class name ,2nd one object(object could be
        dataTypes.printMyName();                   // anything) and 3rd one constructor
        String returndata =dataTypes.getMyName();
        System.out.println(returndata);
        int returnnum =dataTypes.getNumber();
        System.out.println(returnnum);
        System.out.println(name);
    }
}
