package coremodule.constructor;

public class PhoneExecution {

    public static void main(String[] args) {
        Phone phone = new Phone();// this one print null,because don't put any value
        Phone phone1 = new Phone("Samsung",2019);
        phone.printMyCompany();
        phone1.printMyCompany();
        phone1.printMyYear();
        phone1.printMyCompanyAndYear();
        // static variable can call by class name no need to constructor/object
        Phone.getModel();
    }
}
