package coremodule.constructor;

public class Phone {
   static String model = "iphone";
    String company;
    int year =2017;

    public Phone(){

    }
    // parameterize constructor
    public Phone(String company,int year){
        this.company = company;
        this.year= year;

    }
    public void printMyCompany(){
        System.out.println(company);
    }
    public void printMyYear(){
        System.out.println(year);
    }
    public void printMyCompanyAndYear(){
        System.out.println(company);
        System.out.println(year);
    }
    public static void getModel(){
        System.out.println(model);
    }
}
