package coremodule.accessspecifier;

public class ExecutionClass {
    public static void main(String[] args) {
        TestClass testClass = new TestClass();
        String namefromtestclass =testClass.name;
        System.out.println(namefromtestclass);
        String modelfromtestclass =testClass.model;
        System.out.println(modelfromtestclass);
    }
    public class innerClass{
            public void innerMethod () {
                TestClass testClass = new TestClass();
                int dat = testClass.date;
                System.out.println(dat);
            }


    }
}
