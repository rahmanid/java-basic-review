package coremodule.accessspecifier;

public class TestClass {
    // global variable
    String name = "Ahmed";
    public String location = "Brooklyn";
    private String dob = "102019";
    protected String model = "Asus";
    protected int date = 121212;
    // public can call everywhere
    // private can call same class only
    //default can call same class,different class in same package.
    // protected can call same class,different class in same package and
    //also can call inner class in different class in same package
    public void information(){
        // local variable
        String year = "2020";
        System.out.println(year);
        System.out.println(name);
        System.out.println(dob);
    }

}
